const express = require('express');
const app = express();

app.get('/', (req, res) => {
	res.set({'Content-type': 'text/plain'}).send('');
});

app.get('/:context', (req, res) => {
	res.send(req.params.context);
});

app.listen(8000);
