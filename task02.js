const express = require('express');
const app = express();

const Vigenere = require('caesar-salad').Vigenere;
// const password = Vigenere.Cipher('password').crypt('password');

app.get('/', (req, res) => {
	res.set({'Content-type': 'text/plain'}).send('');
});

app.get('/encode/:password', (req, res) => {
	if (req.params.password === 'password') {
		console.log('True');
	}

	res.send(Vigenere.Cipher(req.params.password).crypt(req.params.password));
});

app.get('/decode/:password', (req, res) => {
	res.send(Vigenere.Decipher(req.params.password).crypt(req.params.password));
});

app.listen(8000);